import React from 'react';
import {Route} from "react-router-dom";
import HomePage from "./components/pages/HomePage";
import Header from "./components/fragments/header";
import Footer from "./components/fragments/footer";
import Content from "./components/fragments/content";
import './index.css';
import WomensizePage from './components/pages/WomensizePage';
import MensizePage from './components/pages/MensizePage';
import ClotheDisplay from './components/pages/ClotheDisplay';
import { Container } from "semantic-ui-react";

class App extends React.Component{
  render(){
    return(
      <div >
        <Header />
        <Content>
          <Route path="/"  exact component={HomePage} />
          <Route path="/women"  exact component={WomensizePage} />
          <Route path="/men"  exact component={MensizePage} />
          <Route path="/clothes/:gender/:size"  exact component={ClotheDisplay} />
        </Content>
        <Footer />
      </div>
    );
  }
}

export default App;
