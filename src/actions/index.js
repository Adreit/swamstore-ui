const BASE_URL = "https://swamstore.herokuapp.com/api/apparel";

function getSize(url, data) {
  return fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(async (res) => {
    if (res.status >= 400) {
      let errors = await res.json();
      throw errors;
    } else {
      return res.json();
    }
  })
}

function getMenSize(data) {
  let url = `${BASE_URL}/sizeMen`;
  return getSize(url, data);
}

function getWomenSize(data) {
  let url = `${BASE_URL}/sizeWomen`;
  return getSize(url, data);
}

function getClothes(size, gender, offset=0) {
  let url = `${BASE_URL}/clothes/?size=${size}&gender=${gender}&offset=${offset}&limit=10`;
  return fetch(url, {
    method: "GET"
  }).then(res => res.json())
}

export {
  getMenSize,
  getWomenSize,
  getClothes
}
