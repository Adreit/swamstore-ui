import React from 'react';
import SizeForm from "../forms/SizeForm"
import Title from '../fragments/title';


class WomenSizePage extends React.Component{
  onSubmit (size) {
    this.props.history.push(`/clothes/women/${size}`);
  }

  render () {
    return(
      <div>
        <Title>Please enter your body measurements in inches</Title>
        <SizeForm userType={SizeForm.FORM_USERS.WOMEN} onSubmit={this.onSubmit.bind(this)} />
      </div>
    );
  }
}

export default WomenSizePage;
