import React from 'react';
import { Form, Button, Message, Grid, Image, Table, Menu } from "semantic-ui-react";
import { getMenSize, getWomenSize, getClothes  } from '../../actions';
import Title from '../fragments/title';
import { error } from 'util';


class ClotheDisplay extends React.Component{
  state = {
    data: {
      count: 0,
      next: null,
      previous: null,
      results: []
    },
    loading: false,
    currentPage: -1,
    size: '',
    gender: ''
  }

  componentDidMount() {
    const {size, gender} = this.props.match.params;
    this.setState({loading: true, size, gender});

    getClothes(size, gender)
      .then(res => this.setState({loading: false, data: res, currentPage: 1}));
  }

  onChangePage(pageNumber) {
    this.setState({loading: true});

    getClothes(this.state.size, this.state.gender, (pageNumber - 1) * 10)
      .then(res => this.setState({loading: false, data: res, currentPage: pageNumber}));
  }

  renderProducts() {
    return this.state.data.results.map((item, index) => {
      let availabilityClasses = item.availability ? "availability in" : "availability out";

      return (
        <Table.Row key={index}>
          <Table.Cell>
            <Image className="product-image" src={item.photo} fluid />
          </Table.Cell>
          <Table.Cell className="product-details">
            <h3>{item.name}</h3>
            <div>
              <p>{item.description}</p>
              <div>Price: ${item.price}</div>
              <div className={availabilityClasses}>
                Remaining: {item.availability}
              </div>
              <a class="ui primary button store-url" href={item.url}>Visit Store</a>
            </div>
          </Table.Cell>
        </Table.Row>
      )
    });
  }

  renderPagination() {
    const pagesCount = this.state.data.count / 10;
    if (pagesCount <= 1) {
      return;
    }

    const pages = [];

    for(let i = 1; i <= pagesCount; i ++) {
      pages.push((
        <Menu.Item as='a' onClick={this.onChangePage.bind(this, i)} active={i == this.state.currentPage}>{i}</Menu.Item>
      ))
    }

    return (
      <Table.Footer>
        <Table.Row>
          <Table.HeaderCell colSpan='3'>
            <Menu floated='right' pagination>
              {pages}
            </Menu>
          </Table.HeaderCell>
        </Table.Row>
      </Table.Footer>
    )
  }

  renderResults() {
    if (this.state.data.results.length == 0) {
      return null;
    }

    return (
      <Table>
          <Table.Body>
            {this.renderProducts()}
          </Table.Body>
          {this.renderPagination()}
      </Table>
    );
  }

  renderMessage() {
    if (this.state.loading) {
      return (
        <Message info>
          <Message.Header>Loading...</Message.Header>
        </Message>
      );
    } else {
      if (this.state.data.results.length == 0) {
        return (
          <Message negative>
            <Message.Header>Results not found</Message.Header>
            <p>Sorry, we were not able to find clothes with size: {this.props.match.params.size}.</p>
          </Message>
        );
      } else {
        return (
          <Title>
            Showing clothes for selected size: {this.props.match.params.size}
          </Title>
        );
      }
    }
  }

  render() {
    return (
      <div>
        {this.renderMessage()}
        {this.renderResults()}
      </div>
    )
  }
}
export default ClotheDisplay;