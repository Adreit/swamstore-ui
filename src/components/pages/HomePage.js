import React from 'react';
import { Button } from 'semantic-ui-react';

class HomePage extends React.Component{
  navigate(path) {
    this.props.history.push(path);
  }

  render(){
    return(
      <div className="home-page"> 
        <div>
        {/* Don't worry!
            Shopping online seems to be tricky when you don't know  precisely your size.
            As consequences, you end up ordering clothe that doesn't fit you. */} 
          <p>
            Having trouble determining your size accurately for shooping online? This application 
            allows getting your true size by just take your real body measurements and 
            determining your actual standard size.This helps you ordering clothes that do fit you.
             Then, we show you clothes for your standard size.
          </p>
          <p>
            Get started by selecting your gender below
          </p>
          <div>
            <div>
              <Button secondary onClick={this.navigate.bind(this, '/men')}>Men</Button>
            </div>
            <div>
              <Button secondary onClick={this.navigate.bind(this, '/women')}>Women</Button>
            </div>
          </div>
        </div> 
      </div>
    );
  }
}
  
export default HomePage;
