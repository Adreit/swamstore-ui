import React from 'react';

class Title extends React.Component {
  render() {
    return (
      <div className="page-title">
        <h3>
          {this.props.children}
        </h3>
      </div>
    );
  }
}

export default Title;