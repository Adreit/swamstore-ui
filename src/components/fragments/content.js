import React from 'react';
import { Container } from 'semantic-ui-react'

class Content extends React.Component {
  render() {
    return (
      <div className="store-content">
        <Container>
          {this.props.children}
        </Container>
      </div>
    )
  }
}

export default Content;