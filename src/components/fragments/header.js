import React, { Component } from 'react'
import { Menu, Container } from 'semantic-ui-react'
import { Link } from 'react-router-dom';

class Header extends Component {
  state = {}

  render() {
    return (
      <div className="store-header">
        <Container>
          <Menu>
            <Menu.Item header>
              <Link to="/">SwamStore</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/men">Men</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/women">Women</Link>
            </Menu.Item>
          </Menu>
        </Container>
      </div>
    );
  }
}

export default Header;