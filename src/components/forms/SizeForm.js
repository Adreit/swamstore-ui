import React from 'react';
import { Form, Button, Message, Grid, List } from "semantic-ui-react";
import 'isomorphic-fetch';
import mensize from './mensize.png';
import womensize from './womensize.jpg';
import { getMenSize, getWomenSize } from '../../actions';

const regex = /^[0-9]+(\.[0-9]+)?$/;

const renderErrorItem = (fieldName, fieldError) => {
  return [
    <span style={{fontWeight: 'bold'}}>{fieldName}: </span>,
    <span>{fieldError}</span>,
  ]
}

const FORM_USERS = {
  MEN: 'men',
  WOMEN: 'women'
};

const IMAGES = {
  [FORM_USERS.MEN]: mensize,
  [FORM_USERS.WOMEN]: womensize
};

const ACTIONS = {
  [FORM_USERS.MEN]: getMenSize,
  [FORM_USERS.WOMEN]: getWomenSize
};

const INITIAL_DATA = {
  [FORM_USERS.MEN]: {
    chest:36.0,
    waist:30.0,
    hip:39.0,
    shoulder:16.0,
    neck:14.0
  },
  [FORM_USERS.WOMEN]: {
    chest:34.0,
    waist:25.0,
    hip:37.0,
    inseam:31.0,
    thigh:21.0
  }
}

const INITIAL_SIZE = {
  [FORM_USERS.MEN]: 'SM',
  [FORM_USERS.WOMEN]: 'SM'
}

class SizeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: INITIAL_DATA[props.userType],
      loading: false,
      size: INITIAL_SIZE[props.userType],
      errors: {},
      serverError: ''
    };
  }

  onChange(e) {
    const data = this.state.data;
    data[e.target.name] = e.target.value;
    this.setState({data, size: '', errors: {}});

    // Call API to get the size if all values are valide
    ACTIONS[this.props.userType](data)
      .then(res => this.setState({size: res.size}))
      .catch(errors => this.setState({errors}));
  }
  
  onSubmit() {
    this.props.onSubmit(this.state.size);
  }

  renderWomenFields() {
    const { errors, data} = this.state;

    return (
      <Form onSubmit= {this.onSubmit}>
        <Form.Field>
          <label htmlFor="chest">Chest </label>
          <input type="number" name="chest" value={data.chest} onChange = {this.onChange.bind(this)} />
        </Form.Field>
        <Form.Field>
          <label htmlFor="waist">Waist </label>
          <input type="number" name="waist" value={data.waist} onChange = {this.onChange.bind(this)} />
        </Form.Field>
        <Form.Field>
          <label htmlFor="hip">Hip </label>
          <input type="number" name="hip" value={data.hip} onChange = {this.onChange.bind(this)} />
        </Form.Field>
        <Form.Field>
          <label htmlFor="inseam">Inseam </label>
          <input type="number" name="inseam" value={data.inseam} onChange = {this.onChange.bind(this)} />
        </Form.Field>
        <Form.Field>
          <label htmlFor="thigh">Thigh</label>
          <input type="number" name="thigh" value={data.thigh} onChange = {this.onChange.bind(this)} />
        </Form.Field>
      </Form>
    );
  }

  renderMenFields() {
    const { errors, data } = this.state;

    return (
      <Form>
        <Form.Field>
          <label htmlFor="neck">Neck </label>
          <input type="number" name="neck" value={data.neck} onChange = {this.onChange.bind(this)}/>
        </Form.Field>
        <Form.Field>
          <label htmlFor="shoulder">Shoulder </label>
          <input type="number" name="shoulder" value={data.shoulder} onChange = {this.onChange.bind(this)}/>
        </Form.Field>
        <Form.Field>
          <label htmlFor="chest">Chest </label>
          <input type="number" name="chest" value={data.chest} onChange = {this.onChange.bind(this)} />
        </Form.Field>
        <Form.Field>
          <label htmlFor="waist">Waist </label>
          <input type="number" name="waist" value={data.waist} onChange = {this.onChange.bind(this)} />
        </Form.Field>
        <Form.Field>
          <label htmlFor="hip">Hip</label>
          <input type="number" name="hip" value={data.hip} onChange = {this.onChange.bind(this)}/>
        </Form.Field>
      </Form>
    );
  }

  renderFormFields() {
    let userType = this.props.userType;
    let method = `render${userType.charAt(0).toUpperCase() + userType.substr(1)}Fields`;
    return this[method]();
  }

  renderSizeResult() {
    if (this.state.size) {
      return (
        <Grid.Row>
          <Grid.Column>
            <Message info>
              <Grid columns={2}>
                <Grid.Column className="size-line">
                  <h5 style={{fontSize: "0.9em"}}>YOUR SIZE IS</h5>
                  <hr style={{color: "#2dc03c", lineHeight: "4px"}}/>
                  <h3>{this.state.size} </h3>
                </Grid.Column>
                <Grid.Column className="size-line">
                  <h5 style={{fontSize: "0.9em"}}>SIZE GUIDE </h5>
                  <hr className="line"/>
                  <p>S: Small </p>
                  <p> M: Medium </p> 
                  <p>L: Large </p>
                  <p>XL: X-Large</p>
                  <p>XXL: XX-Large</p>
                </Grid.Column>
              </Grid>
            </Message>
          </Grid.Column>
        </Grid.Row>
      );
    }

    return null;
  }

  renderSubmitButton() {
    if (this.state.size) {
      return (
        <Grid.Row>
          <Grid.Column>
            <Button secondary onClick={this.onSubmit.bind(this)}>View all {this.state.size} clothes</Button> 
          </Grid.Column>
        </Grid.Row>
      );
    }

    return null;
  }

  renderErrors() {
    if (Object.keys(this.state.errors).length == 0) {
      return;
    }

    let errorMessage;

    if (Object.keys(this.state.errors).length == 1) {
      if (this.state.errors.non_field_errors) {
        errorMessage = this.state.errors.non_field_errors;
      } else {
        const fieldName = Object.keys(this.state.errors)[0];
        errorMessage = renderErrorItem(fieldName, this.state.errors[fieldName]);
      }
    } else {
      errorMessage = (
        <List bulleted>
          {Object.keys(this.state.errors).map((key, index) => {
            return (
              <List.Item key={index}>
                {renderErrorItem(key, this.state.errors[key])}
              </List.Item>
            );
          })}
        </List>
      )
    }

    return (
      <Grid.Row>
        <Grid.Column>
          <Message error>
            {errorMessage}
          </Message>
        </Grid.Column>
      </Grid.Row>
    )
  }

  renderForm() {
    return (
      <Grid.Row columns={2}>
        <Grid.Column width={12}>
          {this.renderFormFields()}
        </Grid.Column>
        <Grid.Column width={4}>
          <img style={{marginTop: "1.3rem"}} src={IMAGES[this.props.userType]} width="100%" height="331" />
        </Grid.Column>
      </Grid.Row>
    )
  }
  
  render(){
    return (
      <div  className="size-form">
        <Grid>
          {this.renderErrors()}
          {this.renderForm()}
          {this.renderSizeResult()}
          {this.renderSubmitButton()}
        </Grid>
      </div>
    );
  }
}

SizeForm.FORM_USERS = FORM_USERS;

export default SizeForm;
